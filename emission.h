#ifndef EMISSION_H
#define EMISSION_H

#include "scene.h"

struct LambertEmissionMaterial : Material {
    static const int TYPEID = 83645;
    
    vec3f le;
    Texture* leTxt;
    
    LambertEmissionMaterial() : Material(TYPEID) { 
        le = one3f;
        leTxt = 0;
    }
};

inline vec3f evalLambertEmission(const vec3f& le, const frame3f& f, const vec3f& wo) {
	if (dot(f.z, wo) <= 0) return zero3f;
	return le;
}

inline vec3f evalLambertEmission(const vec3f& le, const vec3f& wo) {
	if (wo.z <= 0) return zero3f;
	return le;
}

#endif
