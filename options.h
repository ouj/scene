#ifndef SCENE_OPTIONS_H
#define SCENE_OPTIONS_H

namespace options {
    namespace scene {
        const bool envmapsampling = true;
        const bool brdfsampling = true;
    };
};

#endif
