#ifndef SCENEIO_H
#define SCENEIO_H

#include <common/json.h>
#include "scene.h"

// differently then previous designs, the io routines are not directly linked
// to the current scene classes - it is possible to create and manipulate
// scenes in a simpler form by having the format more loose wrt the scenegraph.
// parsing will also be loose in the sense that little is done for error
// checking - again to support easier scripting access

// the main data struture is a json representation of the data.
// json does not include type and pointer sharing mechanism
// this is a good thing and we will do this is a simple manner
// then previously to avoid confusion and make it easier to
// manipulate this from scripts.

// a scene is a collection of objects, all stored in arrays
// in the past there were many complex ways to specify these arrays
// in order to make it easier for humans to edit the files.
// this time around that will not happen and the format will be
// easier to parse - the idea is that nobody should be really editing
// this format unless strickly necessary. we should rely on python
// scripts to generate and manipulate the scene (thus making it easier
// to do it). 

// references and types:
// heavy scene objects (surfaces, shapes, textures, ...) have names.
// to include a reference to these objects, we will simply have mechanisms
// to diretly link to the name of the object, instead of using special syntax.
// types are just encoded in objects as needed using a special variable "type" or "class".
// note though that this is not necessary the scenegraph type (although at the
// beginning it will be - this will ensure that we can change the scenegraph
// easily without changing all our files everytime (as happened in the past).
// if an object as no name, one will be generated automatically

// scene:
// there is no more explicit scene in the new system, but just a loose
// collection of objects. the loader code will provide an easy struct
// to access the loaded data.
// the serialized scene is just an unsorted list of objects that can appear
// in any order (no restriction on reference ordering)
// for readability reasons, we also include special names for typed lists.

// format ---------------
/*
 scene -> {
    type: "scene",
    surfaces: [surface],
    cameras: [camera],
    shapes: [shape],
    ...
 }
 
 surface -> {
    type: "surface",
    name: string,
    material_ref: string,
    shape_ref: string,
    transform_ref: string
 }
 
 light -> {
    type: "light",
    name: string,
    source_ref: string,
    transform_ref: string
 }
 
 material -> {
    type: "material",
    name: string,
    reflectance_ref: string
 }
 
 camera -> {
    type: "camera",
    name: string,
    lens_ref: string,
    transform_ref: string
 }

 shape -> trianglemesh
 reflectance -> lambert
 lens -> perspective
 ransform -> identity | rigid

 trianglemesh -> { type: "trianglemesh", name: string, 
                   pos: [float] | pos_filename: string,
                   face: [int] | face_filename: string, 
                   (norm: [float] | norm_filename: string) 
                   (uv: [float] | uv_filename: string)}
 lambert -> { type: "lambert", name: string, rhod: vec3f, rhodtxt_ref: string }
*/

jsonArray printVec2fArray(const std::vector<vec2f>& array);
jsonArray printVec3fArray(const std::vector<vec3f>& array);
jsonArray printVec3iArray(const std::vector<vec3i>& array);
jsonArray printVec2iArray(const std::vector<vec2i>& array);
jsonArray printFloatArray(const std::vector<float>& array);

std::vector<vec2f> parseVec2fArray(const jsonArray& json);
std::vector<vec3f> parseVec3fArray(const jsonArray& json);
std::vector<vec3i> parseVec3iArray(const jsonArray& json);
std::vector<vec2i> parseVec2iArray(const jsonArray& json);
std::vector<float> parseFloatArray(const jsonArray& json);

SceneSettings parseSceneSettings(const jsonObject& json);
jsonObject printSceneSettings(const SceneSettings& setting);
Scene* parseScene(const jsonObject& json, bool preload = true);
Scene* parseScene(const std::string& text, bool preload = true);
Scene* loadScene(const std::string& filename, bool preload = true);

struct SceneWriteOptions {
    enum ImageMode { IMAGEMODE_NONE, IMAGEMODE_BINARY };
    enum MeshMode { MESHMODE_INLINE, MESHMODE_BINARY, MESHMODE_FILENAMEONLY };
    
    ImageMode imageMode;
    MeshMode meshMode;
};

jsonObject printScene(const Scene* scene); // when printing no files are saved by default

void saveScene(const std::string& filename, const Scene* scene, const SceneWriteOptions& opts);
void deleteScene( Scene *&s );

// disk file manipulation
void load(Shape* shape);
void unload(Shape* shape);
void reload(Shape* shape);
void save(Shape* shape);
void remove(Shape* shape);

int  triangleNum(Shape* shape);

#endif
