#ifndef CAMERA_H
#define CAMERA_H

#include "scene.h"

struct PerspectiveCamera : Camera {
    static const int TYPEID = 88654;
    float aperture, distance, width, height;
    PerspectiveCamera() : Camera(TYPEID) {
        aperture = 0;
        distance = 1;
        width = 1;
        height = 1;
    }
    virtual ~PerspectiveCamera() = default;
};

struct OrthogonalCamera : Camera {
    static const int TYPEID = 88655;
    float width, height;
    OrthogonalCamera() : Camera(TYPEID) {
        width = 1;
        height = 1;
    }
    virtual ~OrthogonalCamera() = default;
};

struct PanoramicCamera : Camera {
    static const int TYPEID = 88656;
    PanoramicCamera() : Camera(TYPEID) {}
    virtual ~PanoramicCamera() = default;
};

struct FisheyeCamera : Camera {
    static const int TYPEID = 88657;
    FisheyeCamera() : Camera(TYPEID) {}
    virtual ~FisheyeCamera() = default;
};

#endif
