#include "sceneio.h"
#include "transform.h"
#include "camera.h"
#include "shape.h"
#include "light.h"
#include "material.h"
#include "emission.h"
#include <common/json.h>
#include <common/binio.h>
#include <common/pam.h>
#include <common/raw.h>
#include <common/stddir.h>
#include <algorithm>
#include <map>
using std::string;

template<typename T> using mapping = std::map<std::string, T>;
struct ParseState {
    mapping<Shape*> shapes;
    mapping<Texture*> textures;
    mapping<Transform*> transforms;
    mapping<Material*> materials;
    mapping<Light*> lights;
    mapping<Camera*> cameras;
    mapping<Surface*> surfaces;
};

template<typename A, typename B>
static inline B _get(const std::map<A,B>& m, const A& v) {
    error_if_not(m.find(v) != m.end(), "map value expected");
    return m.find(v)->second;
}

template<typename T>
inline T* _getref(const mapping<T*>& vals, const std::string& name) {
    typename mapping<T*>::const_iterator it = vals.find(name);
    if (it != vals.end()) return it->second;
	string err_msg = "unknown ref: " + name;
    error(err_msg.c_str());
    return 0;
}

inline frame3f parseFrame3f(const jsonObject& json) {
    frame3f frame;
    frame.x = jsonGet(json, "x").as_vec3f();
    frame.y = jsonGet(json, "y").as_vec3f();
    frame.z = jsonGet(json, "z").as_vec3f();
    frame.o = jsonGet(json, "o").as_vec3f();
    return frame;
}

inline jsonObject printFrame3f(const frame3f& frame) {
    jsonObject json;
    json["x"] = frame.x;
    json["y"] = frame.y;
    json["z"] = frame.z;
    json["o"] = frame.o;
    return json;
}

template<typename A, typename R>
inline A _impl_parseNumberArray(const jsonArray& json, size_t nc) {
    A array;
    error_if_not(json.size() % nc == 0, "wrong number of components in array");
    array.resize(static_cast<int>(json.size()/nc));
    R* ptr = ((R*)(&(array[0])));
    for(size_t i = 0; i < json.size(); i ++) ptr[i] = static_cast<R>(json[i].as_number());
    return array;
}

std::vector<vec2f> parseVec2fArray(const jsonArray& json) { return _impl_parseNumberArray<std::vector<vec2f>,float>(json, 2); }
std::vector<vec3f> parseVec3fArray(const jsonArray& json) { return _impl_parseNumberArray<std::vector<vec3f>,float>(json, 3); }
std::vector<vec3i> parseVec3iArray(const jsonArray& json) { return _impl_parseNumberArray<std::vector<vec3i>,int>(json, 3); }
std::vector<vec2i> parseVec2iArray(const jsonArray& json) { return _impl_parseNumberArray<std::vector<vec2i>,int>(json, 2); }
std::vector<float> parseFloatArray(const jsonArray& json) { return _impl_parseNumberArray<std::vector<float>,float>(json, 1); }

template<typename A, typename R>
inline jsonArray _impl_printNumberArray(const A& array, size_t nc) {
    jsonArray json(array.size()*nc);
    const R* ptr = (const R*)(&(array[0]));
    for(size_t i = 0; i < json.size(); i ++) json[i] = ptr[i];
    return json;
}
    
jsonArray printVec2fArray(const std::vector<vec2f>& array) { return _impl_printNumberArray<std::vector<vec2f>,float>(array,2); }
jsonArray printVec3fArray(const std::vector<vec3f>& array) { return _impl_printNumberArray<std::vector<vec3f>,float>(array,3); }
jsonArray printVec3iArray(const std::vector<vec3i>& array) { return _impl_printNumberArray<std::vector<vec3i>,int>(array,3); }
jsonArray printVec2iArray(const std::vector<vec2i>& array) { return _impl_printNumberArray<std::vector<vec2i>,int>(array,2); }
jsonArray printFloatArray(const std::vector<float>& array) { return _impl_printNumberArray<std::vector<float>,float>(array,1); }

template<typename T>
std::vector<T*> parseSceneObjectArray(const jsonObject& json, const char* name,
                                 T* (*parseFunc)(const jsonObject&, ParseState&), ParseState &ps) {
    if (jsonHas(json, name)) {
        const jsonArray &array = jsonGet(json, name).as_array();
        std::vector<T*> val(static_cast<int>(array.size()));
        for(int i = 0; i < val.size(); i ++) {
            val[i] = parseFunc(array[i].as_object(), ps);
        }
        return val;
    } else return std::vector<T*>();
}

template<typename T>
jsonArray printSceneObjectArray(const std::vector<T*>& val, jsonObject (*printFunc)(const T*, const SceneWriteOptions&),
                                const SceneWriteOptions& opts) {
    jsonArray json(val.size());
    for(int i = 0; i < val.size(); i ++) {
        json[i] = printFunc(val[i], opts);
    }
    return json;
}

Shape* parseShape(const jsonObject& json, ParseState &ps) {
    Shape* val = 0;
    const std::string& type = jsonGet(json, "type").as_string();
    if(type == "trianglemeshshape") {
        TriangleMeshShape* ptr = new TriangleMeshShape();
        ptr->name = jsonGet(json, "name").as_string();
        if(jsonHas(json, "face")) ptr->face = parseVec3iArray(jsonGet(json,"face").as_array());
        else if(jsonHas(json, "face_filename")) ptr->face_filename = jsonGet(json, "face_filename").as_string();
        else error("missing face array");
        if(jsonHas(json, "pos")) ptr->pos = parseVec3fArray(jsonGet(json,"pos").as_array());
        else if(jsonHas(json, "pos_filename")) ptr->pos_filename = jsonGet(json, "pos_filename").as_string();
        else error("missing pos array");
        if(jsonHas(json, "norm")) ptr->norm = parseVec3fArray(jsonGet(json,"norm").as_array());
        else if(jsonHas(json, "norm_filename")) ptr->norm_filename = jsonGet(json, "norm_filename").as_string();
        else ptr->norm.clear();
        if(jsonHas(json, "uv")) ptr->uv = parseVec2fArray(jsonGet(json,"uv").as_array());
        else if(jsonHas(json, "uv_filename")) ptr->uv_filename = jsonGet(json, "uv_filename").as_string();
        else ptr->uv.clear();
        val = ptr;
    } else if(type == "hairmeshshape") {
        HairMeshShape* ptr = new HairMeshShape();
        ptr->name = jsonGet(json, "name").as_string();
        if(jsonHas(json, "face")) ptr->face = parseVec3iArray(jsonGet(json,"face").as_array());
        else if(jsonHas(json, "face_filename")) ptr->face_filename = jsonGet(json, "face_filename").as_string();
        else error("missing face array");
        if(jsonHas(json, "pos")) ptr->pos = parseVec3fArray(jsonGet(json,"pos").as_array());
        else if(jsonHas(json, "pos_filename")) ptr->pos_filename = jsonGet(json, "pos_filename").as_string();
        else error("missing pos array");
        if(jsonHas(json, "tangent")) ptr->tangent = parseVec3fArray(jsonGet(json,"tangent").as_array());
        else if(jsonHas(json, "tangent_filename")) ptr->tangent_filename = jsonGet(json, "tangent_filename").as_string();
        else error("missing tangent array");
        if(jsonHas(json, "binormal")) ptr->tangent = parseVec3fArray(jsonGet(json,"binormal").as_array());
        else if(jsonHas(json, "binormal_filename")) ptr->binormal_filename = jsonGet(json, "binormal_filename").as_string();
        if(jsonHas(json, "uv")) ptr->uv = parseVec2fArray(jsonGet(json,"uv").as_array());
        else if(jsonHas(json, "uv_filename")) ptr->uv_filename = jsonGet(json, "uv_filename").as_string();
        else ptr->uv.clear();
        val = ptr;
    } else if(type == "quadshape") {
        QuadShape* ptr = new QuadShape();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->width = jsonGet(json, "width").as_float();
        ptr->height = jsonGet(json, "height").as_float();
        val = ptr;
    } else if(type == "sphereshape") {
        SphereShape* ptr = new SphereShape();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->radius = jsonGet(json, "radius").as_float();
        val = ptr;
    } else error("unknown shape type");
    ps.shapes[val->name] = val;
    return val;
}

jsonObject printShape(const Shape* val, const SceneWriteOptions& opts) {
    jsonObject json;
    if(val->type == TriangleMeshShape::TYPEID) {
        const TriangleMeshShape* ptr = (const TriangleMeshShape*)val;
        json["type"] = "trianglemeshshape";
        json["name"] = ptr->name;
        if (opts.meshMode == SceneWriteOptions::MESHMODE_INLINE) {
            json["face"] = printVec3iArray(ptr->face);
            json["pos"] = printVec3fArray(ptr->pos);
            if(!ptr->norm.empty()) json["norm"] = printVec3fArray(ptr->norm);
            if(!ptr->uv.empty()) json["uv"] = printVec2fArray(ptr->uv);
        } else {
            if(ptr->face_filename.empty()) json["face"] = printVec3iArray(ptr->face);
            else json["face_filename"] =  ptr->face_filename;
            if(ptr->pos_filename.empty()) json["pos"] = printVec3fArray(ptr->pos);
            else json["pos_filename"] =  ptr->pos_filename;
            if(!ptr->norm.empty()) {
                if(ptr->norm_filename.empty()) json["norm"] = printVec3fArray(ptr->norm);
                else json["norm_filename"] = ptr->norm_filename;
            }
            if(!ptr->uv.empty()) { 
                if(ptr->uv_filename.empty()) printVec2fArray(ptr->uv);
                else json["uv_filename"] = ptr->uv_filename;
            }
            if (opts.meshMode != SceneWriteOptions::MESHMODE_FILENAMEONLY) {
                if(jsonHas(json, "face_filename")) saveRaw(json["face_filename"].as_string(), ptr->face);
                if(jsonHas(json, "pos_filename")) saveRaw(json["pos_filename"].as_string(), ptr->pos);
                if(!ptr->norm.empty() && jsonHas(json, "norm_filename")) 
                    saveRaw(json["norm_filename"].as_string(), ptr->norm);
                if(!ptr->uv.empty() && jsonHas(json, "uv_filename")) 
                    saveRaw(json["uv_filename"].as_string(), ptr->uv);
            }
        }
    } else if (val->type == HairMeshShape::TYPEID) {
        const HairMeshShape* ptr = (const HairMeshShape*)val;
        json["type"] = "hairmeshshape";
        json["name"] = ptr->name;
        if (opts.meshMode == SceneWriteOptions::MESHMODE_INLINE) {
            json["face"] = printVec3iArray(ptr->face);
            json["pos"] = printVec3fArray(ptr->pos);
            json["tangent"] = printVec3fArray(ptr->tangent);
            if(!ptr->binormal.empty()) json["binormal"] = printVec3fArray(ptr->binormal);
            if(!ptr->uv.empty()) json["uv"] = printVec2fArray(ptr->uv);
        } else {
            if(ptr->face_filename.empty()) json["face"] = printVec3iArray(ptr->face);
            else json["face_filename"] =  ptr->face_filename;
            if(ptr->pos_filename.empty()) json["pos"] = printVec3fArray(ptr->pos);
            else json["pos_filename"] =  ptr->pos_filename;
            if(ptr->tangent_filename.empty()) json["tangent"] = printVec3fArray(ptr->tangent);
            else json["tangent_filename"] = ptr->tangent_filename;
            if(!ptr->binormal.empty()) {
                if(ptr->binormal_filename.empty()) json["binormal"] = printVec3fArray(ptr->binormal);
                else json["binormal_filename"] = ptr->binormal_filename;
            }
            if(!ptr->uv.empty()) {
                if(ptr->uv_filename.empty()) json["uv"] = printVec2fArray(ptr->uv);
                    json["uv_filename"] = ptr->uv_filename;
            }
            if (opts.meshMode != SceneWriteOptions::MESHMODE_FILENAMEONLY) {
                if(jsonHas(json, "face_filename")) saveRaw(json["face_filename"].as_string(), ptr->face);
                if(jsonHas(json, "pos_filename")) saveRaw(json["pos_filename"].as_string(), ptr->pos);
                if(jsonHas(json, "tangent_filename")) saveRaw(json["tangent_filename"].as_string(), ptr->tangent);
                if(!ptr->binormal.empty() && jsonHas(json, "binormal_filename")) 
                    saveRaw(json["binormal_filename"].as_string(), ptr->binormal);
                if(!ptr->uv.empty() && jsonHas(json, "uv_filename")) 
                    saveRaw(json["uv_filename"].as_string(), ptr->uv);
            }
        }
        if(!ptr->uv.empty()) json["uv"] = printVec2fArray(ptr->uv);
    } else if(val->type == QuadShape::TYPEID) {
        const QuadShape* ptr = (const QuadShape*)val;
        json["type"] = "quadshape";
        json["name"] = ptr->name;
        json["width"] = ptr->width;
        json["height"] = ptr->height;
    } else if(val->type == SphereShape::TYPEID) {
        const SphereShape* ptr = (const SphereShape*)val;
        json["type"] = "sphereshape";
        json["name"] = ptr->name;
        json["radius"] = ptr->radius;
    } else error("unknown shape type");
    return json;
}

void load(Shape* shape) {
    if (shape->type == TriangleMeshShape::TYPEID) {
        TriangleMeshShape* ptr = (TriangleMeshShape*)shape;
        if (!ptr->face_filename.empty() && ptr->face.empty()) loadRaw(ptr->face_filename, ptr->face);
        if (!ptr->pos_filename.empty() && ptr->pos.empty()) loadRaw(ptr->pos_filename, ptr->pos);
        if (!ptr->norm_filename.empty() && ptr->norm.empty()) loadRaw(ptr->norm_filename, ptr->norm);
        if (!ptr->uv_filename.empty() && ptr->uv.empty()) loadRaw(ptr->uv_filename, ptr->uv);
    } else if (shape->type == HairMeshShape::TYPEID) {
        HairMeshShape* ptr = (HairMeshShape*)shape;
        if (!ptr->face_filename.empty() && ptr->face.empty()) loadRaw(ptr->face_filename, ptr->face);
        if (!ptr->pos_filename.empty() && ptr->pos.empty()) loadRaw(ptr->pos_filename, ptr->pos);
        if (!ptr->tangent_filename.empty() && ptr->tangent.empty()) loadRaw(ptr->tangent_filename, ptr->tangent);
        if (!ptr->binormal_filename.empty() && ptr->binormal.empty()) loadRaw(ptr->binormal_filename, ptr->binormal);
        if (!ptr->uv_filename.empty() && ptr->uv.empty()) loadRaw(ptr->uv_filename, ptr->uv);
    }
}

void unload(Shape* shape) {
    if (shape->type == TriangleMeshShape::TYPEID) {
        TriangleMeshShape* ptr = (TriangleMeshShape*)shape;
        if (!ptr->face_filename.empty()) { ptr->face.clear(); ptr->face.shrink_to_fit(); }
        if (!ptr->pos_filename.empty()) { ptr->pos.clear(); ptr->pos.shrink_to_fit(); }
        if (!ptr->norm_filename.empty()) { ptr->norm.clear(); ptr->norm.shrink_to_fit(); }
        if (!ptr->uv_filename.empty()) { ptr->uv.clear(); ptr->uv.shrink_to_fit(); }
    } else if (shape->type == HairMeshShape::TYPEID) {
        HairMeshShape* ptr = (HairMeshShape*)shape;
        if (!ptr->face_filename.empty()) { ptr->face.clear(); ptr->face.shrink_to_fit(); }
        if (!ptr->pos_filename.empty()) { ptr->pos.clear(); ptr->pos.shrink_to_fit(); }
        if (!ptr->tangent_filename.empty()) { ptr->tangent.clear(); ptr->tangent.shrink_to_fit(); }
        if (!ptr->binormal_filename.empty()) { ptr->binormal.clear(); ptr->binormal.shrink_to_fit(); }
        if (!ptr->uv_filename.empty()) { ptr->uv.clear(); ptr->uv.shrink_to_fit(); }
    }
}

void save(Shape* shape) {
    if (shape->type == TriangleMeshShape::TYPEID) {
        TriangleMeshShape* ptr = (TriangleMeshShape*)shape;
        if (!ptr->face_filename.empty() && !ptr->face.empty()) saveRaw(ptr->face_filename, ptr->face);
        if (!ptr->pos_filename.empty() && !ptr->pos.empty()) saveRaw(ptr->pos_filename, ptr->pos);
        if (!ptr->norm_filename.empty() && !ptr->norm.empty()) saveRaw(ptr->norm_filename, ptr->norm);
        if (!ptr->uv_filename.empty() && !ptr->uv.empty()) saveRaw(ptr->uv_filename, ptr->uv);
    } else if (shape->type == HairMeshShape::TYPEID) {
        HairMeshShape* ptr = (HairMeshShape*)shape;
        if (!ptr->face_filename.empty() && !ptr->face.empty()) saveRaw(ptr->face_filename, ptr->face);
        if (!ptr->pos_filename.empty() && !ptr->pos.empty()) saveRaw(ptr->pos_filename, ptr->pos);
        if (!ptr->tangent_filename.empty() && !ptr->tangent.empty()) saveRaw(ptr->tangent_filename, ptr->tangent);
        if (!ptr->binormal_filename.empty() && !ptr->binormal.empty()) saveRaw(ptr->binormal_filename, ptr->binormal);
        if (!ptr->uv_filename.empty() && !ptr->uv.empty()) saveRaw(ptr->uv_filename, ptr->uv);
    }
}

void remove(Shape* shape) {
    if (shape->type == TriangleMeshShape::TYPEID) {
        TriangleMeshShape* ptr = (TriangleMeshShape*)shape;
        if (!ptr->face_filename.empty()) { remove(ptr->face_filename.c_str()); }
        if (!ptr->pos_filename.empty()) { remove(ptr->pos_filename.c_str()); }
        if (!ptr->norm_filename.empty()) { remove(ptr->norm_filename.c_str()); }
        if (!ptr->uv_filename.empty()) { remove(ptr->uv_filename.c_str()); }
    } else if (shape->type == HairMeshShape::TYPEID) {
        HairMeshShape* ptr = (HairMeshShape*)shape;
        if (!ptr->face_filename.empty()) { remove(ptr->face_filename.c_str()); }
        if (!ptr->pos_filename.empty()) { remove(ptr->pos_filename.c_str()); }
        if (!ptr->tangent_filename.empty()) { remove(ptr->tangent_filename.c_str()); }
        if (!ptr->binormal_filename.empty()) { remove(ptr->binormal_filename.c_str()); }
        if (!ptr->uv_filename.empty()) { remove(ptr->uv_filename.c_str()); }
    }
}

void reload(Shape* shape) {
    unload(shape);
    load(shape);
}

int triangleNum(Shape* shape) { //not tested
    if (shape->type == TriangleMeshShape::TYPEID) {
        TriangleMeshShape* ptr = (TriangleMeshShape*)shape;
        if (!ptr->face.empty()) return ptr->face.size();
        else if(!ptr->face_filename.empty()) {
            int size1, size2, nc;
            char type;
            loadDim(ptr->face_filename, type, size1, size2, nc);
            error_if(nc!=3 || type != 'i', "invalid dimensioin");
            return size1 * size2;
        } else return 0;
    } else if (shape->type == HairMeshShape::TYPEID) {
        HairMeshShape* ptr = (HairMeshShape*)shape;
        if (!ptr->face.empty()) return ptr->face.size();
        else if(!ptr->face_filename.empty()) {
            int size1, size2, nc;
            char type;
            loadDim(ptr->face_filename, type, size1, size2, nc);
            error_if(nc!=3 || type != 'i', "invalid dimensioin");
            return size1 * size2;
        } else return 0;
    }
    return 0;
}

Transform* parseTransform(const jsonObject& json, ParseState &ps) {
    Transform* val = 0; 
    const string& type = jsonGet(json, "type").as_string();
    if(type == "identitytransform") {
        IdentityTransform* ptr = new IdentityTransform();
        ptr->name = jsonGet(json, "name").as_string();
        val = ptr;
    } else if(type == "rigidtransform") {
        RigidTransform* ptr = new RigidTransform();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->frame = parseFrame3f(jsonGet(json, "frame").as_object());
        val = ptr;
    } else error("unknown transform type");
    ps.transforms[val->name] = val;
    return val;
}

jsonObject printTransform(const Transform* val, const SceneWriteOptions& opts) {
    jsonObject json;
    if(val->type == IdentityTransform::TYPEID) {
        const IdentityTransform* ptr = (const IdentityTransform*)val;
        json["type"] = "identitytransform";
        json["name"] = ptr->name;
    } else if(val->type == RigidTransform::TYPEID) {
        const RigidTransform* ptr = (const RigidTransform*)val;
        json["type"] = "rigidtransform";
        json["name"] = ptr->name;
        json["frame"] = printFrame3f(ptr->frame);
    } else error("unknown transform type");
    return json;
}

Texture* parseTexture(const jsonObject& json, ParseState &ps) {
    Texture* val = 0;
    const string& type = jsonGet(json, "type").as_string();
    if(type == "texture") {
        Texture* ptr = new Texture();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->image_filename = jsonGet(json, "image_filename").as_string();
        ptr->image = loadPnm3f(jsonGet(json, "image_filename").as_string());
		const string& wrap = jsonGet(json, "wrap").as_string();
		if (wrap == "repeat")
			ptr->wrap = Texture::REPEAT;
		else if (wrap == "clamp")
			ptr->wrap = Texture::CLAMP;
		else if (wrap == "black")
			ptr->wrap = Texture::BLACK;
		else
			error("unknown texture wrap mode");
        val = ptr;
    } else error("unknown texture type");
    ps.textures[val->name] = val;
    return val;
}

jsonObject printTexture(const Texture* val, const SceneWriteOptions& opts) {
    jsonObject json;
    json["type"] = "texture";
    json["name"] = val->name;
    json["image_filename"] = val->image_filename;
    if (val->wrap == Texture::REPEAT) {
        json["wrap"] = "repeat";
    } else if (val->wrap == Texture::BLACK) {
        json["wrap"] = "black";
    } else if (val->wrap == Texture::CLAMP) {
        json["wrap"] = "clamp";
    } else {
        error("unknown texture wrapping");
    }
    switch(opts.imageMode) {
        case SceneWriteOptions::IMAGEMODE_NONE: break;
        case SceneWriteOptions::IMAGEMODE_BINARY: 
            savePfm(val->image_filename, val->image);
            break;
        default: error("unknown image mode");
    }
    return json;
}
    
void _parseSpatialVariation(const jsonObject& json, const string& name, 
                            vec3f& v, Texture*& txt, const ParseState &ps) {
    v = jsonGet(json, name).as_vec3f();
    if(jsonHas(json, name+"txt_ref")) {
        txt = _getref(ps.textures, jsonGet(json, name+"txt_ref").as_string());
    } else txt = 0;
}
    
void _parseSpatialVariation(const jsonObject& json, const string& name, 
                            float& v, Texture*& txt, const ParseState &ps) {
    v = jsonGet(json, name).as_float();
    if(jsonHas(json, name+"txt_ref")) {
        txt = _getref(ps.textures, jsonGet(json, name+"txt_ref").as_string());
    } else txt = 0;
}

void _printSpatialVariation(jsonObject& json, const string& name, 
                            const vec3f& v, Texture* txt) {        
    json[name] = v;
    if(txt) json[name+"txt_ref"] = txt->name;
}    
    
void _printSpatialVariation(jsonObject& json, const string& name, 
                            float v, Texture* txt) {        
    json[name] = v;
    if(txt) json[name+"txt_ref"] = txt->name;
}    

Light* parseLight(const jsonObject& json, ParseState &ps) {
    Light* val = 0;
    const string& type = jsonGet(json, "type").as_string();
    if(type == "pointlight") {
        PointLight* ptr = new PointLight();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->intensity = jsonGet(json, "intensity").as_vec3f();
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;
    } else if(type == "directionallight") {
        DirectionalLight* ptr = new DirectionalLight();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->radiance = jsonGet(json, "radiance").as_vec3f();
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;            
    } else if(type == "arealight") {
        AreaLight* ptr = new AreaLight();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->material = _getref(ps.materials, jsonGet(json, "material_ref").as_string());
        ptr->shape = _getref(ps.shapes, jsonGet(json, "shape_ref").as_string());
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;            
    } else if(type == "environmentlight") {
        EnvironmentLight* ptr = new EnvironmentLight();
        ptr->name = jsonGet(json, "name").as_string();
        _parseSpatialVariation(json, "le", ptr->le, ptr->leTxt, ps);
        ptr->traceAux = 0;
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;            
    } else error("unknown light type");
    ps.lights[val->name] = val;
    return val;
}

jsonObject printLight(const Light* val, const SceneWriteOptions& opts) {
    jsonObject json;
    if(val->type == PointLight::TYPEID) {
        const PointLight* ptr = (const PointLight*)val;
        json["type"] = "pointlight";
        json["name"] = ptr->name;
        json["intensity"] = ptr->intensity;
        json["transform_ref"] = val->transform->name;
    } else if(val->type == DirectionalLight::TYPEID) {
        const DirectionalLight* ptr = (const DirectionalLight*)val;
        json["type"] = "directionallight";
        json["name"] = ptr->name;
        json["radiance"] = ptr->radiance;
        json["transform_ref"] = val->transform->name;
    } else if(val->type == AreaLight::TYPEID) {
        const AreaLight* ptr = (const AreaLight*)val;
        json["type"] = "arealight";
        json["name"] = ptr->name;
        json["material_ref"] = ptr->material->name;
        json["shape_ref"] = ptr->shape->name;
        json["transform_ref"] = val->transform->name;
    } else if(val->type == EnvironmentLight::TYPEID) {
        const EnvironmentLight* ptr = (const EnvironmentLight*)val;
        json["type"] = "environmentlight";
        json["name"] = ptr->name;
        _printSpatialVariation(json, "le", ptr->le, ptr->leTxt);
        json["transform_ref"] = val->transform->name;
    } else error("unknown light type");
    return json;
}

/*
Reflectance* parseReflectance(const jsonObject& json, ParseState &ps) {
    Reflectance* val = 0;
    const string& type = jsonGet(json, "type").as_string();
    if(type == "LambertMaterial") {
        LambertMaterial* ptr = new LambertMaterial();
        ptr->name = jsonGet(json, "name").as_string();
        _parseSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt, ps);
        val = ptr;
    } else if(type == "MicrofacetMaterial") {
        MicrofacetMaterial* ptr = new MicrofacetMaterial();
        ptr->name = jsonGet(json, "name").as_string();
        _parseSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt, ps);
        _parseSpatialVariation(json, "rhos", ptr->rhos, ptr->rhosTxt, ps);
        _parseSpatialVariation(json, "n", ptr->n, ptr->nTxt, ps);
        std::map<std::string, int> diffuseModes;
        diffuseModes["lambert"] = MDM_LAMBERT;
        diffuseModes["ashikminshirley"] = MDM_ASHIKMINSHIRLEY;
        ptr->diffuseMode = _get(diffuseModes, jsonGet(json, "diffuse").as_string());
        std::map<std::string, int> specularModes;
        specularModes["blinnphong"] = MSM_BLINNPHONG;
        specularModes["cooktorrance"] = MSM_COOKTORRANCE;
        specularModes["ashikmonshirley"] = MSM_ASHIKMINSHIRLEY;
        ptr->specularMode = _get(specularModes, jsonGet(json, "specular").as_string());
        val = ptr;
    } else if(type == "KajiyaHairMaterial") {
        KajiyaHairMaterial* ptr = new KajiyaHairMaterial();
        ptr->name = jsonGet(json, "name").as_string();
        _parseSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt, ps);
        val = ptr;
    } else error("unknown reflectance type");
    ps.reflectances[val->name] = val;
    return val;
}

jsonObject printReflectance(const Reflectance* val, const SceneWriteOptions& opts) {
    jsonObject json;
    if(val->type == LambertMaterial::TYPEID) {
        const LambertMaterial* ptr = (const LambertMaterial*)val;
        json["type"] = "lambermaterial";
        json["name"] = ptr->name;
        _printSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt);
    } else if(val->type == MicrofacetMaterial::TYPEID) {
        const MicrofacetMaterial* ptr = (const MicrofacetMaterial*)val;
        json["type"] = "MicrofacetMaterial";
        json["name"] = ptr->name;
        _printSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt);
        _printSpatialVariation(json, "rhos", ptr->rhos, ptr->rhosTxt);
        _printSpatialVariation(json, "n", ptr->n, ptr->nTxt);
        std::map<int, std::string> diffuseModes;
        diffuseModes[MDM_LAMBERT] = "lambert";
        diffuseModes[MDM_ASHIKMINSHIRLEY] = "ashikminshirley";
        json["diffuse"] = _get(diffuseModes, ptr->diffuseMode);
        std::map<int, std::string> specularModes;
        specularModes[MSM_BLINNPHONG] = "blinnphong";
        specularModes[MSM_COOKTORRANCE] = "cooktorrance";
        specularModes[MSM_ASHIKMINSHIRLEY] = "ashikmonshirley";
        json["specular"] = _get(specularModes, ptr->specularMode);
    } else if(val->type == KajiyaHairMaterial::TYPEID) {
        const KajiyaHairMaterial* ptr = (const KajiyaHairMaterial*)val;
        json["type"] = "KajiyaHairMaterial";
        json["name"] = ptr->name;
        _printSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt);
    } else error("unknown reflectance type");
    return json;
}

Emission* parseEmission(const jsonObject& json, ParseState &ps) {
    Emission* val = 0;
    const string& type = jsonGet(json, "type").as_string();
    if(type == "lambertemission") {
        LambertEmission* ptr = new LambertEmission();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->le = jsonGet(json, "le").as_vec3f();
        if(jsonHas(json, "letxt_ref")) {
            ptr->leTxt = _getref(ps.textures, jsonGet(json, "letxt_ref").as_string());
        } else ptr->leTxt = 0;
        val = ptr;
    } else error("unknown emission type");
    ps.emissions[val->name] = val;
    return val;
}

jsonObject printEmission(const Emission* val, const SceneWriteOptions& opts) {
    jsonObject json;
    if(val->type == LambertEmission::TYPEID) {
        const LambertEmission* ptr = (const LambertEmission*)val;
        json["type"] = "lambertemission";
        json["name"] = ptr->name;
        json["le"] = ptr->le;
        if(ptr->leTxt) json["letxt_ref"] = ptr->leTxt->name;
    } else error("unknown emission type");
    return json;
}
 */

Material* parseMaterial(const jsonObject& json, ParseState &ps) {
    Material* val = 0;
    const string& type = jsonGet(json, "type").as_string();
    if(type == "lambertmaterial") {
        LambertMaterial* ptr = new LambertMaterial();
        ptr->name = jsonGet(json, "name").as_string();
        _parseSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt, ps);
        val = ptr;
    } else if (type == "microfacetmaterial") {
        MicrofacetMaterial* ptr = new MicrofacetMaterial();
        ptr->name = jsonGet(json, "name").as_string();
        _parseSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt, ps);
        _parseSpatialVariation(json, "rhos", ptr->rhos, ptr->rhosTxt, ps);
        _parseSpatialVariation(json, "n", ptr->n, ptr->nTxt, ps);
        const std::string &dm = jsonGet(json, "diffuse").as_string();
        if (dm == "lambert") ptr->diffuseMode = MDM_LAMBERT;
        else if (dm == "ashikminshirley") ptr->diffuseMode = MDM_ASHIKMINSHIRLEY;
        const std::string &sm = jsonGet(json, "specular").as_string();
        if (sm == "blinnphong") ptr->specularMode = MSM_BLINNPHONG;
        else if (sm == "cooktorrance") ptr->specularMode = MSM_COOKTORRANCE;
        else if (sm == "ashikmonshirley") ptr->specularMode = MSM_ASHIKMINSHIRLEY;
        val = ptr;
    } else if (type == "kajiyahairmaterial") {
        KajiyaHairMaterial* ptr = new KajiyaHairMaterial();
        ptr->name = jsonGet(json, "name").as_string();
        _parseSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt, ps);
        val = ptr;
    } else if (type == "lambertemissionmaterial") {
        LambertEmissionMaterial* ptr = new LambertEmissionMaterial();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->le = jsonGet(json, "le").as_vec3f();
        if(jsonHas(json, "letxt_ref")) {
            ptr->leTxt = _getref(ps.textures, jsonGet(json, "letxt_ref").as_string());
        } else ptr->leTxt = 0;
        val = ptr;
    } else error("unknown material type");
    if(jsonHas(json, "opacity_ref")) val->opacity = _getref(ps.textures, jsonGet(json, "opacity_ref").as_string());
    if(jsonHas(json, "bump_ref")) val->bump = _getref(ps.textures, jsonGet(json, "bump_ref").as_string());
    ps.materials[val->name] = val;
    return val;
}

jsonObject printMaterial(const Material* val, const SceneWriteOptions& opts) {
    jsonObject json;
    if (val->opacity) json["opacity_ref"] = val->opacity->name;
    if (val->bump) json["bump_ref"] = val->bump->name;
    if (val->type == LambertEmissionMaterial::TYPEID) {
        const LambertEmissionMaterial* ptr = (const LambertEmissionMaterial*)val;
        json["type"] = "lambertemissionmaterial";
        json["name"] = ptr->name;
        json["le"] = ptr->le;
        if(ptr->leTxt) json["letxt_ref"] = ptr->leTxt->name;
    } else if (val->type == MicrofacetMaterial::TYPEID) {
        const MicrofacetMaterial* ptr = (const MicrofacetMaterial*)val;
        json["type"] = "microfacetmaterial";
        json["name"] = ptr->name;
        _printSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt);
        _printSpatialVariation(json, "rhos", ptr->rhos, ptr->rhosTxt);
        _printSpatialVariation(json, "n", ptr->n, ptr->nTxt);

        if (ptr->diffuseMode == MDM_LAMBERT) {
            json["diffuse"] = "lambert";
        } else if (ptr->diffuseMode == MDM_ASHIKMINSHIRLEY) {
            json["diffuse"] = "ashikminshirley";
        } else error("invalid diffuse mode");
        
        if (ptr->specularMode == MSM_BLINNPHONG) {
            json["specular"] = "blinnphong";
        } else if (ptr->specularMode == MSM_COOKTORRANCE) {
            json["specular"] = "cooktorrance";
        } else if (ptr->specularMode == MSM_ASHIKMINSHIRLEY) {
            json["specular"] = "ashikmonshirley";
        } else error("invalid specular mode");
    } else if (val->type == KajiyaHairMaterial::TYPEID) {
        const KajiyaHairMaterial* ptr = (const KajiyaHairMaterial*)val;
        json["type"] = "kajiyahairmaterial";
        json["name"] = ptr->name;
        _printSpatialVariation(json, "rhod", ptr->rhod, ptr->rhodTxt);
    } else error("unknown material type");
    return json;
}

Camera* parseCamera(const jsonObject& json, ParseState &ps) {
    Camera* val = 0;
    const string& type = jsonGet(json, "type").as_string();
    if(type == "perspectivecamera") {
        PerspectiveCamera* ptr = new PerspectiveCamera();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->aperture = jsonGet(json, "aperture").as_float();
        ptr->distance = jsonGet(json, "distance").as_float();
        ptr->width = jsonGet(json, "width").as_float();
        ptr->height = jsonGet(json, "height").as_float();
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;
    } else if (type == "orthogonalcamera") {
        OrthogonalCamera* ptr = new OrthogonalCamera();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->width = jsonGet(json, "width");
        ptr->height = jsonGet(json, "height");
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;
    } else if (type == "panoramiccamera") {
        PanoramicCamera* ptr = new PanoramicCamera();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;
    } else if (type == "fisheyecamera") {
        FisheyeCamera* ptr = new FisheyeCamera();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;
    } else error("unknown camera type");
    ps.cameras[val->name] = val;
    return val;
}

jsonObject printCamera(const Camera* val, const SceneWriteOptions& opts) {
    jsonObject json;
    if(val->type == PerspectiveCamera::TYPEID) {
        const PerspectiveCamera* ptr = (const PerspectiveCamera*)val;
        json["type"] = "perspectivecamera";
        json["name"] = ptr->name;
        json["aperture"] = ptr->aperture;
        json["distance"] = ptr->distance;
        json["width"] = ptr->width;
        json["height"] = ptr->height;
        json["transform_ref"] = val->transform->name;
    } else if (val->type == OrthogonalCamera::TYPEID) {
        const OrthogonalCamera* ptr = (const OrthogonalCamera*)val;
        json["type"] = "orthogonalcamera";
        json["name"] = ptr->name;
        json["width"] = ptr->width;
        json["height"] = ptr->height;
        json["transform_ref"] = val->transform->name;
    } else if (val->type == PanoramicCamera::TYPEID) {
        const PanoramicCamera* ptr = (const PanoramicCamera*)val;
        json["type"] = "panoramiccamera";
        json["name"] = ptr->name;
        json["transform_ref"] = val->transform->name;
    } else if (val->type == FisheyeCamera::TYPEID) {
        const FisheyeCamera* ptr = (const FisheyeCamera*)val;
        json["type"] = "fisheyecamera";
        json["name"] = ptr->name;
        json["transform_ref"] = val->transform->name;
    } else error("unknown lens type");
    return json;
}

Surface* parseSurface(const jsonObject& json, ParseState &ps) {
    Surface* val = 0; 
    const string& type = jsonGet(json, "type").as_string();
    if(type == "surface") {
        Surface* ptr = new Surface();
        ptr->name = jsonGet(json, "name").as_string();
        ptr->shape = _getref(ps.shapes, jsonGet(json, "shape_ref").as_string());
        ptr->material = _getref(ps.materials, jsonGet(json, "material_ref").as_string());
        ptr->transform = _getref(ps.transforms, jsonGet(json, "transform_ref").as_string());
        val = ptr;
    } else error("unknown surface type");
    ps.surfaces[val->name] = val;
    return val;
}

jsonObject printSurface(const Surface* val, const SceneWriteOptions& opts) {
    jsonObject json;
    json["type"] = "surface";
    json["name"] = val->name;
    json["shape_ref"] = val->shape->name;
    json["material_ref"] = val->material->name;
    json["transform_ref"] = val->transform->name;
    return json;
}

Scene* parseScene(const jsonObject& json, bool preload) {
    Scene* val = new Scene();
    ParseState ps;
    
    // parse scene arrays one after another
    if(jsonHas(json, "settings")) val->settings = parseSceneSettings(jsonGet(json, "settings").as_object());
    val->textures = parseSceneObjectArray(json, "textures", parseTexture, ps);
    val->transforms = parseSceneObjectArray(json, "transforms", parseTransform, ps);
    val->shapes = parseSceneObjectArray(json, "shapes", parseShape, ps);
    val->materials = parseSceneObjectArray(json, "materials", parseMaterial, ps);
    val->lights = parseSceneObjectArray(json, "lights", parseLight, ps);
    val->cameras = parseSceneObjectArray(json, "cameras", parseCamera, ps);
    val->surfaces = parseSceneObjectArray(json, "surfaces", parseSurface, ps);
    
    if (preload) std::for_each(val->shapes.begin(), val->shapes.end(), [](Shape* s){load(s);});
    return val;
}

jsonObject printScene(const Scene* val, const SceneWriteOptions& opts) {
    jsonObject json;
    json["settings"] = printSceneSettings(val->settings);
    json["textures"] = printSceneObjectArray(val->textures, printTexture, opts);
    json["transforms"] = printSceneObjectArray(val->transforms, printTransform, opts);
    json["shapes"] = printSceneObjectArray(val->shapes, printShape, opts);
    json["materials"] = printSceneObjectArray(val->materials, printMaterial, opts);
    json["lights"] = printSceneObjectArray(val->lights, printLight, opts);
    json["cameras"] = printSceneObjectArray(val->cameras, printCamera, opts);
    json["surfaces"] = printSceneObjectArray(val->surfaces, printSurface, opts);
    return json;
}

inline void mergeArray(const string &name,
                       jsonObject &json, const jsonObject &include_json) {
    if (!jsonHas(json, name)) {
        json[name] = jsonArray();
    }
    if (jsonHas(include_json, name)) {
        jsonArray &a = json.at(name).as_array_ref();
        const jsonArray &b = include_json.at(name).as_array_ref();
        a.insert(a.end(), b.begin(), b.end());
    }
}

void mergeJson(jsonObject &json, const jsonObject &include_json) {
    mergeArray("textures", json, include_json);
    mergeArray("transforms", json, include_json);
    mergeArray("shapes", json, include_json);
    mergeArray("lenses", json, include_json);
    mergeArray("emissions", json, include_json);
    mergeArray("reflectances", json, include_json);
    mergeArray("materials", json, include_json);
    mergeArray("sources", json, include_json);
    mergeArray("lights", json, include_json);
    mergeArray("cameras", json, include_json);
    mergeArray("surfaces", json, include_json);
}

// recursively handle includes
void loadIncludes(jsonObject &json) {
    if (jsonHas(json, "includes")) {
        const jsonArray &jsonIncludes = jsonGet(json, "includes").as_array();
        for (int i = 0; i < jsonIncludes.size(); i++) {
            const string& include_file = jsonIncludes[i];
            string ext = getFileExtension(include_file);
            jsonObject include_json;
            if (ext == "json") {
                include_json = loadJson(include_file).as_object_ref();
            } else if (ext == "jbin") {
                include_json = loadBin(include_file).as_object_ref();
            }
            loadIncludes(include_json);
            mergeJson(json, include_json);
        }
    }
}

Scene* parseScene(const string& text, bool preload) {
    jsonObject json = parseJson(text).as_object();
    return parseScene(json, preload);
}

Scene* loadScene(const string& filename, bool preload) {
    string ext = getFileExtension(filename);
    jsonObject json;
    if (ext == "json") {
        json = loadJson(filename).as_object_ref();
    }
    else if(ext == "jbin") {
        json = loadBin(filename).as_object_ref();
    }
    else {
        error("unkown file extension");
        return 0;
    }
    loadIncludes(json);
    return parseScene(json, preload);
}

void saveScene(const string& filename, const Scene* scene, const SceneWriteOptions& opts) {
    jsonObject json = printScene(scene, opts);
    string ext = getFileExtension(filename);
    if (ext == "json")
        saveJson(filename, json);
    else if(ext == "jbin") 
        saveBin(filename, json);
    else
        error("unkown file extension");
}

SceneSettings parseSceneSettings(const jsonObject& json) {
    SceneSettings setting;
    setting.rayepsilon = jsonGet(json, "rayepsilon");
    return setting;
}

jsonObject printSceneSettings(const SceneSettings& setting) {
    jsonObject json;
    json["rayepsilon"] = setting.rayepsilon;
    return json;
}

template<typename T>
void deleteSceneObjectArray(std::vector<T*> &a) {
	for (int i = 0; i < a.size(); i++) delete a[i];
	a.clear();
}

void deleteScene( Scene *&s ) {
	if(s == 0) return;
    deleteSceneObjectArray(s->shapes);
    deleteSceneObjectArray(s->transforms);
    deleteSceneObjectArray(s->textures);
    deleteSceneObjectArray(s->materials);
    deleteSceneObjectArray(s->lights);
    deleteSceneObjectArray(s->cameras);
	deleteSceneObjectArray(s->surfaces);
	delete s;
	s = 0;
}
