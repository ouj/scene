#include "material.h"

vec3f evalLambertReflectance(const vec3f& rho, const frame3f& f, const vec3f& wi, const vec3f& wo) {
	if (dot(f.z, wo) <= 0 || dot(f.z, wi) <= 0) return zero3f;
	return rho * consts<float>::inv_pi;
}

vec3f evalLambertReflectance(const vec3f& rho, const vec3f& wil, const vec3f& wol) {
    if(wil.z <= 0 || wol.z <= 0) return zero3f;
    return rho * consts<float>::inv_pi;
}
    
vec3f evalKajiyaHairReflectance(const vec3f& rho, const frame3f& f, const vec3f& wi, const vec3f& wo) {
    float cost = dot(f.z, wi);
    float sint = sqrtf(1 - cost * cost);
    vec3f bsdf = rho * sint / consts<float>::twopi; 
    return bsdf / cost; // cheat the projection
}
    
vec3f evalKajiyaHairReflectance(const vec3f& rho, const vec3f& wil, const vec3f& wol) {
    float cost = wil.z;
    float sint = sqrtf(1 - cost * cost);
    vec3f bsdf = rho * sint / consts<float>::twopi; 
    return bsdf / cost; // cheat the projection
}

vec3f schlickFresnel(const vec3f& rhos, float iDh) {
    return rhos + (one3f-rhos) * pow(1.0f-iDh,5.0f);
}

vec3f schlickFresnel(const vec3f& rhos, const vec3f& w, const vec3f& wh) {
    return schlickFresnel(rhos, dot(wh,w));
}

vec3f _impl_evalMicrofacetReflectance(const vec3f& rhod, const vec3f& rhos, float n,
                                      int diffuseMode, int specularMode,
                                      float nDo, float nDi, float nDh, float iDh) {
    vec3f diffuse = zero3f;
    if(diffuseMode == MDM_LAMBERT) {
        diffuse = rhod * consts<float>::inv_pi;
    } else if (diffuseMode == MDM_ASHIKMINSHIRLEY) {
        diffuse = rhod * (one3f - rhos) *
        ((28.0f/(23.0f*consts<float>::pi)) * (1-powf(1-nDi/2,5)) * (1-powf(1-nDo/2,5)));
    } else error("unknown diffuse mode");
    
    vec3f specular = zero3f;
    if(specularMode == MSM_BLINNPHONG) {
        specular = rhos * ( (n+2) * pow(nDh,n) / (2 * consts<float>::pi) );
    } else if(specularMode == MSM_COOKTORRANCE) {
        float r = 1 / n;
        vec3f f = schlickFresnel(rhos, iDh);
        float d = expf((nDh*nDh-1)/(r*r*nDh*nDh)) / (r*r*pow(nDh,4));
        float g = min(1.0f, min((2*nDh*nDo/iDh), (2*nDh*nDi/iDh)));
        specular = rhos * f * (d * g / (4*nDo*nDi)); 
    } else if (specularMode == MSM_ASHIKMINSHIRLEY) {
        vec3f f = schlickFresnel(rhos, iDh);
        float d = ( (n+1) * pow(nDh,n) / (2 * consts<float>::pi) );
        specular = rhos * f * (d / (4*nDi*max(nDi,nDo)));
    } else error("unknown specular mode");
    
    return diffuse + specular;
}

vec3f evalMicrofacetReflectance(const vec3f& rhod, const vec3f& rhos, float n,
                                int diffuseMode, int specularMode,
                                const frame3f& f, const vec3f& wi, const vec3f& wo) {
	if (dot(f.z, wo) <= 0 || dot(f.z, wi) <= 0) return zero3f;
    
    vec3f wh = normalize(wo+wi);
    float nDo = dot(wo, f.z);
    float nDi = dot(wi, f.z);
    float nDh = dot(wh, f.z);
    float iDh = dot(wi, wh);
    
    return _impl_evalMicrofacetReflectance(rhod,rhos,n,
                                           diffuseMode,specularMode,
                                           nDo,nDi,nDh,iDh);
}

vec3f evalMicrofacetReflectance(const vec3f& rhod, const vec3f& rhos, float n,
                                int diffuseMode, int specularMode,
                                const vec3f& wil, const vec3f& wol) {
	if (wol.z <= 0 || wil.z <= 0) return zero3f;
    
    vec3f whl = normalize(wol+wil);
    float nDo = wol.z;
    float nDi = wil.z;
    float nDh = whl.z;
    float iDh = dot(wil, whl);
    
    return _impl_evalMicrofacetReflectance(rhod,rhos,n,
                                           diffuseMode,specularMode,
                                           nDo,nDi,nDh,iDh);
}
