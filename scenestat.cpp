#include "scenestat.h"
#include "shape.h"
#include "transform.h"
#include "material.h"
#include "camera.h"
#include "emission.h"
#include "light.h"

jsonObject statistics(Scene* scene) {
    jsonObject json;
    uint64_t triNum = getTriangleNumber(scene->surfaces);
    uint64_t bytes = getMemoryUsage(scene);
    json["triangle-number"] = triNum;
    json["memory-usage(mb)"] = byteToMB(bytes);
    return json;
}

    
uint64_t getTriangleNumber(const std::vector<Surface*> &surfaces) {
    uint64_t faceNum = 0;
    for (int s = 0; s < surfaces.size(); s++) {
        const Surface* surface = surfaces[s];
        const Shape* shape = surface->shape;
        if (shape->type == TriangleMeshShape::TYPEID) {
            const TriangleMeshShape* triMesh = (const TriangleMeshShape*)shape;
            faceNum += triMesh->face.size();
        }
    }
    return faceNum;
}

uint64_t getMemoryUsage(const Shape* shape) {
    uint64_t bytes = 0;
    bytes += getMemoryUsage(shape->name);
    switch (shape->type) {
        case TriangleMeshShape::TYPEID: {
            TriangleMeshShape* triShape = (TriangleMeshShape*)shape;
            bytes += sizeof(TriangleMeshShape);
            bytes += getMemoryUsage(triShape->pos);
            bytes += getMemoryUsage(triShape->face);
            bytes += getMemoryUsage(triShape->norm);
            bytes += getMemoryUsage(triShape->uv);
        } break;
        case QuadShape::TYPEID: {
            bytes += sizeof(QuadShape);
        } break;
        case SphereShape::TYPEID: {
            bytes += sizeof(SphereShape);
        } break;
        case HairMeshShape::TYPEID: {
            HairMeshShape* hair = (HairMeshShape*)shape;
            bytes += sizeof(HairMeshShape);
            bytes += getMemoryUsage(hair->face);
            bytes += getMemoryUsage(hair->pos);
            bytes += getMemoryUsage(hair->tangent);
            bytes += getMemoryUsage(hair->binormal);
            bytes += getMemoryUsage(hair->uv);
        } break;
        default: error("unknown shape type in getMemoryUsage()");
    }
    return bytes;
}

uint64_t getMemoryUsage(const std::vector<Shape*> &shapes) {
    uint64_t bytes = 0;
    for (int s = 0; s < shapes.size(); s++) {
        Shape* shape = shapes[s];
        bytes += getMemoryUsage(shape);
    }
    return bytes;
}

uint64_t getMemoryUsage(const std::vector<Texture*> &textures) {
    uint64_t bytes = 0;
    for (int t = 0; t < textures.size(); t++) {
        Texture *tex = textures[t];
        bytes += sizeof(Texture);
        bytes += getMemoryUsage(tex->name);
        bytes += getMemoryUsage(tex->image);
    }
    return bytes;
}

uint64_t getMemoryUsage(const std::vector<Transform*> &transforms) {
    uint64_t bytes = 0;
    for (int t = 0; t < transforms.size(); t++) {
        Transform *transform = transforms[t];
        bytes += getMemoryUsage(transform->name);
        switch (transform->type) {
            case IdentityTransform::TYPEID:
                bytes += sizeof(IdentityTransform);
                break;
            case RigidTransform::TYPEID:
                bytes += sizeof(RigidTransform);
                break;
            default: error("unknown transform type in getMemoryUsage()");
        }
    }
    return bytes;
}

uint64_t getMemoryUsage(const std::vector<Camera*> &cameras) {
    uint64_t bytes = 0;
    for (int l = 0; l < cameras.size(); l++) {
        Camera *cam = cameras[l];
        bytes += getMemoryUsage(cam->name);
        switch (cam->type) {
            case PerspectiveCamera::TYPEID:
                bytes += sizeof(PerspectiveCamera);
                break;
            case OrthogonalCamera::TYPEID:
                bytes += sizeof(OrthogonalCamera);
                break;
            case PanoramicCamera::TYPEID:
                bytes += sizeof(PanoramicCamera);
                break;
            case FisheyeCamera::TYPEID:
                bytes += sizeof(FisheyeCamera);
                break;
            default: error("unknown camera type in getMemoryUsage()");
        }
    }
    return bytes;
}

uint64_t getMemoryUsage(const std::vector<Material*> &materials) {
    uint64_t bytes = 0;
    for (int i = 0; i < materials.size(); i++) {
        Material *material = materials[i];
        bytes += getMemoryUsage(material->name);
        bytes += sizeof(Material);
    }
    return bytes;
}

uint64_t getMemoryUsage(const std::vector<Light*> &lights) {
    uint64_t bytes = 0;
    for (int i = 0; i < lights.size(); i++) {
        Light *light = lights[i];
        bytes += getMemoryUsage(light->name);
        switch (light->type) {
            case PointLight::TYPEID:
                bytes += sizeof(PointLight);
                break;
            case DirectionalLight::TYPEID:
                bytes += sizeof(DirectionalLight);
                break;
            case AreaLight::TYPEID:
                bytes += sizeof(AreaLight);
                break;
            case EnvironmentLight::TYPEID:
                bytes += sizeof(EnvironmentLight);
                break;
            default: error("unknown light type in getMemoryUsage()");
        }
    }
    return bytes;    
}

uint64_t getMemoryUsage(const std::vector<Surface*> &surfaces) {
    uint64_t bytes = 0;
    for (int i = 0; i < surfaces.size(); i++) {
        Surface *surface = surfaces[i];
        bytes += getMemoryUsage(surface->name);
        bytes += sizeof(Surface);
    }
    return bytes;
}

uint64_t getMemoryUsage(const Scene* scene) {
    uint64_t bytes = 0;
    bytes += getMemoryUsage(scene->shapes);
    bytes += getMemoryUsage(scene->textures);
    bytes += getMemoryUsage(scene->transforms);
    bytes += getMemoryUsage(scene->materials);
    bytes += getMemoryUsage(scene->lights);
    bytes += getMemoryUsage(scene->cameras);
    bytes += getMemoryUsage(scene->surfaces);
    return bytes;
}

