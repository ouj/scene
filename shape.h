#ifndef SHAPE_H
#define SHAPE_H

#include "scene.h"

struct TriangleMeshShape : Shape {
    static const int TYPEID = 876;

    std::vector<vec3i> face;
    std::vector<vec3f> pos;
    std::vector<vec3f> norm;
    std::vector<vec2f> uv;

    std::string face_filename;
    std::string pos_filename;
    std::string norm_filename;
    std::string uv_filename;

    TriangleMeshShape() : Shape(TYPEID) { }
    virtual ~TriangleMeshShape() {}
};

struct QuadShape : Shape {
	static const int TYPEID = 877;

	float width, height;
    
    QuadShape() : Shape(TYPEID) { 
        width = 1; 
        height = 1; 
    }
    virtual ~QuadShape() {}
};

struct SphereShape : Shape {
    static const int TYPEID = 878;
    
    float radius;
    
    SphereShape() : Shape(TYPEID) { 
        radius = 1; 
    }
    virtual ~SphereShape() {}
};

    
struct HairMeshShape : Shape {
    static const int TYPEID = 879;
    
    std::vector<vec3i> face;
    std::vector<vec3f> pos;
    std::vector<vec3f> tangent;
    std::vector<vec3f> binormal;
    std::vector<vec2f> uv;

    std::string face_filename;
    std::string pos_filename;
    std::string tangent_filename;
    std::string binormal_filename;
    std::string uv_filename;

    HairMeshShape() : Shape(TYPEID) { }
    virtual ~HairMeshShape() {}
};

#endif
