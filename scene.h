#ifndef _SCENE_H_
#define _SCENE_H_

// basic math
#include <string>
#include "common/stdmath.h"
#include "common/vec.h"
#include "common/range.h"
#include "common/ray.h"
#include "common/frame.h"
#include "common/random.h"
#include "common/func.h"
#include "common/geom.h"
#include "common/array.h"
#include "common/debug.h"


// declarations for extended types
struct Shape { 
    const int           type; 
    std::string         name; 
    Shape(int t) : type(t) { }
    virtual ~Shape() {};
};

struct Transform { 
    const int           type; 
    std::string         name; 
    Transform(int t) : type(t) { }
    virtual ~Transform() {};
};

struct TraceAux { };

// texture
struct Texture { 
    enum WrapMode { REPEAT, CLAMP, BLACK };
    std::string         name;
    Image<vec3f>        image;
    WrapMode            wrap;
    std::string         image_filename;
};

// declaration for container types
struct Camera {
    const int           type; 
    std::string         name;
    Transform*          transform; 
    Camera(int t) : type(t), transform(0) { }
    virtual ~Camera() {};
};

struct Light {
    const int           type;
    std::string         name;
    Transform*          transform; 
    Light(int t) : type(t), transform(0) { }
    virtual ~Light() {};
};

struct Material {
    const int           type;
    std::string         name;
    Texture*            opacity; 
    Texture*            bump;
    Material(int t) : type(t), opacity(0), bump(0) { }
    virtual ~Material() {};
};

struct Surface {
    std::string         name;
    Shape*              shape; 
    Material*           material; 
    Transform*          transform;
    Surface() : shape(0), material(0), transform(0) { }
    virtual ~Surface() {};
};

struct SceneSettings {
    SceneSettings() { rayepsilon = 1e-5f; }
    float rayepsilon;
};

struct Scene {
    SceneSettings                settings;
    std::vector<Shape*>          shapes;
    std::vector<Texture*>        textures;
    std::vector<Transform*>      transforms;
    std::vector<Material*>       materials;
    std::vector<Light*>          lights;
    std::vector<Camera*>         cameras;
    std::vector<Surface*>        surfaces;
};

#endif
