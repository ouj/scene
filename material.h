#ifndef REFLECTANCE_H
#define REFLECTANCE_H

#include "scene.h"

struct LambertMaterial : Material {
    static const int TYPEID = 73645;
    
    vec3f rhod;
    Texture* rhodTxt;
    
    LambertMaterial() : Material(TYPEID) { 
        rhod = one3f; 
        rhodTxt = 0; 
    }
    virtual ~LambertMaterial() = default;
};

const int MDM_LAMBERT = 1;
const int MDM_ASHIKMINSHIRLEY = 2;

const int MSM_BLINNPHONG  = 1;
const int MSM_ASHIKMINSHIRLEY = 2;
const int MSM_COOKTORRANCE = 3;

// the sum of two lobes: a diffuse and a specular one
// TODO: different fresnel modes
struct MicrofacetMaterial : Material {
    static const int TYPEID = 73646;
    
    vec3f rhod;
    vec3f rhos; 
    float n;
    Texture* rhodTxt;
    Texture* rhosTxt;
    Texture* nTxt;
    int diffuseMode, specularMode;
    
    MicrofacetMaterial() : Material(TYPEID) {
        rhod = one3f * 0.5f; 
        rhos = one3f * 0.5f;
        n = 100;
        rhodTxt = 0; 
        rhosTxt = 0;
        nTxt = 0;
        diffuseMode = MDM_LAMBERT;
        specularMode = MSM_BLINNPHONG;
    }
    virtual ~MicrofacetMaterial() = default;
};

struct KajiyaHairMaterial : Material {
    static const int TYPEID = 73647;
    
    vec3f rhod;
    Texture* rhodTxt;
    
    KajiyaHairMaterial() : Material(TYPEID) {
        rhod = one3f;
        rhodTxt = 0;
    }
    virtual ~KajiyaHairMaterial() { }
};

vec3f evalLambertReflectance(const vec3f& rho, const frame3f& f, const vec3f& wi, const vec3f& wo);
vec3f evalLambertReflectance(const vec3f& rho, const vec3f& wil, const vec3f& wol);
vec3f evalKajiyaHairReflectance(const vec3f& rho, const frame3f& f, const vec3f& wi, const vec3f& wo);
vec3f evalKajiyaHairReflectance(const vec3f& rho, const vec3f& wil, const vec3f& wol);
vec3f evalMicrofacetReflectance(const vec3f& rhod, const vec3f& rhos,
                                float n, int diffuseMode, int specularMode,
                                const frame3f& f, const vec3f& wi, const vec3f& wo);
vec3f evalMicrofacetReflectance(const vec3f& rhod, const vec3f& rhos, float n,
                                int diffuseMode, int specularMode,
                                const vec3f& wil, const vec3f& wol);

#endif
