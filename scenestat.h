#ifndef _SCENE_STAT_H_
#define _SCENE_STAT_H_

#include "scene.h"
#include "sceneio.h"

inline uint64_t getMemoryUsage(const std::string &str) {
    return sizeof(std::string::value_type) * str.size();
}

template <typename T>
inline uint64_t getMemoryUsage(const std::vector<T> &array) {
    return sizeof(T) * array.size();
}

template<typename T>
inline uint64_t getMemoryUsage(const Image<T> &array) {
    return sizeof(T) * array.width() * array.height();
}

// scene structure indepencent statistic functions
uint64_t getTriangleNumber(const std::vector<Surface*> &surfaces);
uint64_t getMemoryUsage(const Shape* shape);
uint64_t getMemoryUsage(const std::vector<Shape*> &shapes);
uint64_t getMemoryUsage(const std::vector<Texture*> &textures);
uint64_t getMemoryUsage(const std::vector<Transform*> &transforms);
uint64_t getMemoryUsage(const std::vector<Material*> &materials);
uint64_t getMemoryUsage(const std::vector<Light*> &sources);
uint64_t getMemoryUsage(const std::vector<Camera*> &cameras);
uint64_t getMemoryUsage(const std::vector<Surface*> &surfaces);

// scene structure dependent statistic functions
uint64_t getMemoryUsage(const Scene* scene);
jsonObject statistics(Scene* scene);

#endif
