#ifndef LIGHT_H
#define LIGHT_H

#include "scene.h"

struct PointLight : Light {
    static const int TYPEID = 447733;
    vec3f intensity;
    PointLight() : Light(TYPEID) {
        intensity = one3f; 
    }
    virtual ~PointLight() {}
};

struct DirectionalLight : Light {
    static const int TYPEID = 447734;
    vec3f radiance;
    DirectionalLight() : Light(TYPEID) {
        radiance = one3f; 
    }
    virtual ~DirectionalLight() {}
};

struct AreaLight : Light {
    static const int TYPEID = 447735;
    Material* material;
    Shape* shape;
    AreaLight() : Light(TYPEID) {
        material = 0; 
        shape = 0; 
    }
    virtual ~AreaLight() {}
};

struct EnvironmentLight : Light {
    static const int TYPEID = 447736;
    vec3f le;
    Texture* leTxt;
    TraceAux* traceAux;
    EnvironmentLight() : Light(TYPEID) {
        le = one3f; 
        leTxt = 0; 
        traceAux = 0; 
    }
    virtual ~EnvironmentLight() {}
};

#endif
