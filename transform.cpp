#include "transform.h"

ray3f transformRay(const Transform* transform, const ray3f& ray) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return ray;
    case RigidTransform::TYPEID: 
        return ((RigidTransform*)transform)->frame.transformRay(ray);
    default: error("Unknown transform type."); return ray;
    }
}
ray3f transformRayInverse(const Transform* transform, const ray3f& ray) {
    switch (transform->type) {
    case IdentityTransform::TYPEID: return ray;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformRayInverse(ray);
    default: error("Unknown transform type"); return ray;
    }
}
vec3f transformPoint(const Transform* transform, const vec3f& p) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return p;
    case RigidTransform::TYPEID: 
        return ((RigidTransform*)transform)->frame.transformPoint(p);
    default: error("Unknown transform type."); return p;
    }
}
vec3f transformPointInverse(const Transform* transform, const vec3f& p) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return p;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformPointInverse(p);
    default: error("Unknown transform type."); return p;
    }
}
vec3f transformNormal(const Transform* transform, const vec3f& n) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return n;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformNormal(n);
    default: error("Unknown transform type."); return n;
    }
}
vec3f transformNormalInverse(const Transform* transform, const vec3f& n) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return n;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformNormalInverse(n);
    default: error("Unknown transform type."); return n;
    }
}
vec3f transformVector(const Transform* transform, const vec3f& v) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return v;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformVector(v);
    default: error("Unknown transform type."); return v;
    }
}
vec3f transformVectorInverse(const Transform* transform, const vec3f& p) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return p;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformVectorInverse(p);
    default: error("Unknown transform type."); return p;
    }
}
frame3f transformFrame(const Transform* transform, const frame3f& f) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return f;
    case RigidTransform::TYPEID: 
        return ((RigidTransform*)transform)->frame.transformFrame(f);
    default: error("Unknown transform type."); return f;
    }
}

range3f transformBBox(const Transform *transform, const range3f &b ) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return b;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformRange(b);
    default: error("Unknown transform type."); return b;
    }
}

range3f transformBBoxInverse(const Transform *transform, const range3f &b ) {
    switch(transform->type) {
    case IdentityTransform::TYPEID: return b;
    case RigidTransform::TYPEID:
        return ((RigidTransform*)transform)->frame.transformRangeInverse(b);
    default: error("Unknown transform type."); return b;
    }
}

