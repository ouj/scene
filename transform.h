#ifndef XFORM_H
#define XFORM_H
#undef near
#include "scene.h"

struct IdentityTransform : Transform {
    static const int TYPEID = 123;
    IdentityTransform() : Transform(TYPEID) { }
    virtual ~IdentityTransform() = default;
};

struct RigidTransform : Transform {
    static const int TYPEID = 124;
    frame3f frame;
    RigidTransform() : Transform(TYPEID), frame(defaultframe3f) {};
    RigidTransform(const frame3f &f) : Transform(TYPEID), frame(f) {};
    virtual ~RigidTransform() = default;
};

ray3f transformRay(const Transform* transform, const ray3f& ray);
ray3f transformRayInverse(const Transform* transform, const ray3f& ray);
vec3f transformPoint(const Transform* transform, const vec3f& p);
vec3f transformPointInverse(const Transform* transform, const vec3f& p);
vec3f transformNormal(const Transform* transform, const vec3f& n);
vec3f transformNormalInverse(const Transform* transform, const vec3f& n);
vec3f transformVector(const Transform* transform, const vec3f& v);
vec3f transformVectorInverse(const Transform* transform, const vec3f& p);
frame3f transformFrame(const Transform* transform, const frame3f& f);
range3f transformBBox(const Transform *transform, const range3f &b );
range3f transformBBoxInverse(const Transform *transform, const range3f &b );

#endif
